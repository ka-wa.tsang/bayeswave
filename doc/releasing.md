# Preparing A Release
Please familiarise yourself with this entire procedure before starting.

 1. [Update Hardcoded Version Info](#update-hardcoded-version-info)
 1. [Create a tag](#create-a-tag)
 1. [Build A Release Tarball](#build-a-release-tarball)
 1. [upload Source Tarball](#upload-source-tarball)
 1. [Open SCCB Request](#open-sccb-request)
 1. [Deployment](#deployment)

## Update Hardcoded Version Info
There are a couple of places with (currently) hardcoded version numbers.
Update these with the version you're about to release.  
Specifically, edit :
 * `conf.py` [here](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/conf.py#L25).
 * `CMakeLists.txt` [here](https://git.ligo.org/lscsoft/bayeswave/blob/master/CMakeLists.txt#L13)

Future releases may automate this step.

## Create a tag
Tag the state of the repository you'd like to release: [create new tag in
gitlab](https://git.ligo.org/lscsoft/bayeswave/-/tags/new). See [previous
tags](https://git.ligo.org/lscsoft/bayeswave/-/tags) for examples of notes and
the semantic version name used (e.g. vX.Y.Z)

## Build A Source Tarball
LSC software is released by uploading a source tarball to the
[software.ligo.org](software.ligo.org) software repository.  This tarball can
then be unpacked and built by package managers.  At this time, BayesWave can be
managed by the following systems:
 * `conda`, in which case the BayesWave release will eventually show up in the
 [LIGO conda reference environments](https://docs.ligo.org/lscsoft/conda/).
 * `yum`, in which case BayesWave can be installed directly from the LSCSoft
 SL7 repositories.

The easiest way to enter a suitable build environment is to use the BayesWave
dependencies docker image.  Assuming we're starting at the root of the
BayesWave repository:
```
$ docker run -it -u $(id -u):$(id -g) -w $PWD -v /home:/home containers.ligo.org/lscsoft/bayeswave/dependencies:el7
$ mkdir dist
$ pushd dist
$ cmake3 ..
$ cmake3 --build . --target package_source
```
(Noting that the Cmake executable in the BayesWave dependencies image is called
 `cmake3`).

On success, this should produce a compressed tarball `bayeswave-X.Y.Z.tar.xz`,
where `X.Y.Z` is your tag version.

## Upload Source Tarball
Acquire the upload script `lscsrc_new_file.sh` from UWM.  If using
linux (rather than OSX), make sure you change the call to `gmktemp` to `mktemp`:
```
#SKTFILE=`gmktemp --dry-run`
SKTFILE=`mktemp --dry-run`
```
Run the script to upload the tarball:
```
$ sh ~/Downloads/lscsrc_new_file.sh bayeswave-X.Y.Z.tar.xz
```

## Open SCCB Request
Full instructions available [here](https://docs.ligo.org/sccb/requests/).  Briefly:
 1. [Open a ticket](https://git.ligo.org/sccb/requests/issues/new)
 1. Invite your reviewer to post a comment indicating review sign-off.

## Deployment
The process now leaves the BayesWave team's hands and packages will be built
and deployed using the provided tarball, pending SCCB approval.
